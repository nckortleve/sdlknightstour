/*
    The solution has been modified by Nick Kortleve.
*/

/*

  Filename: knightour.cpp
  Author  : Piyush Kumar

  To Compile: use g++ knightour.cpp -O3 -o knight
  To Run    : ./knight

  The purpose of this program is to demonstrate some simple C++ features.
  (Copy constructor, operator overloading <<, friend function)
  It also shows how to implement simple backtracking/recursive solutions.

  Written for : OOP in C++ Class, COP 3330, FSU.

  Our problem:  From an arbitrary starting position, find a knight's tour for
  a given size grid.

  What is a knight tour? (From wikipedia)

  A knight's tour of a chessboard (or any other grid) is a sequence of moves
  (i.e., a tour) by a knight chess piece (which may only make moves which
  simultaneously shift one square along one axis and two along the other)
  such that each square of the board is visited exactly once
  (i.e., a Hamiltonian path).

  The output is a solution for the knights tour  problem in a sxs chessboard.
  If you find any bugs, please let me know.

  Note: For the 8x8 board, and with first piece at location (7,0),
  it takes about 5 minutes on my home machine (Pentium 4 2.99Ghz).

  Beware: If you are looking for an efficient knightour solver, this solution
  is going to disappoint you. You should try using an explicit stack and less
  space for a more efficient solution.

*/

#include <iostream>
#include <iomanip>
#include <vector>
#include "tour.h"
#include "chessboard.h"

using namespace std;

bool tourIsFinished = false;
int cycles = 0;

// Implementation of the overloaded << which is a friend of tour...
std::ostream& operator<<( std::ostream& os, const tour& T ){
  os << endl;
  int size = T.size;

  os << " +"; for(int i = 0; i < size*5-1; ++i) os << "-";
  os << "+\n";

  for(int i = 0; i < size; ++i){
    os << " | ";
    for(int j = 0; j < size; ++j)
      os <<  setw(2) << T.board[i][j] << " | ";
    os << endl;

    os << " +"; for(int i = 0; i < size*5-1; ++i) os << "-";
    os << "+\n";

  }
  return os;
}


// A recursive function to find the knight tour.
bool tour::findtour(tour& T, int imove){
    if(imove == (size*size - 1)) return true;

    // make a move
	int cx = T.sx;
	int cy = T.sy;
    int cs = T.size;

    if(cycles % 1000 == 0) std::cout << "\r" << "Cycles: " << cycles;
    cycles++;

    for ( int i = 0; i < 8; ++i)
    {
        if(tourIsFinished) return false;
        int tcx = cx + move[i][0];
        int tcy = cy + move[i][1];
        if (
          // Is this a valid move?
                  (tcx >= 0) &&  (tcy >= 0)  &&  (tcx < cs) &&  (tcy < cs) &&
          // Has this place been visited yet?
                  (T.board[tcx][tcy] == -1)
             ){
            tour temp(T);
            temp.board[tcx][tcy] = imove+1;
            temp.sx = tcx;
            temp.sy = tcy;
            if(findtour(temp, imove+1))
            {
              // Stop looking if solution was found.
              if(tourIsFinished)
                return false;
              std::cout << "\r" << "Cycles: " << cycles;
              cout << endl << endl << "Solution found\n";
              cout << temp << endl;
              tourIsFinished = true;
              ConvertSolutionToPoints(temp.board);
            }
        } // legal move?
    } // for

    return false;
}
