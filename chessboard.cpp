#include <iostream>
#include <stdio.h>
#include <string>
#include <vector>

#include "SDL2/SDL.h"

#include "chessboard.h"
#include "tour.h"

SDL_Window *window;
SDL_Renderer *renderer;
SDL_Surface *surf;
SDL_Texture *texture;
SDL_Texture *image;

SDL_Event event;

SDL_Rect tileRect;
SDL_Rect tempTileRect;

std::vector < SDL_Point > cb_points;

//Convert the 2d solution board to a vector of SDL_Point.
void ConvertSolutionToPoints(std::vector< std::vector< int > > cb)
{

    int currentMove = 0;
    while(currentMove < (cb_dim_x * cb_dim_y))
    {
        for(int i = 0; i < cb.size(); i++)
        {
            for(int j = 0; j < cb[0].size(); j++)
            {
                if(cb[i][j] == currentMove)
                {
                    cb_points.push_back ({j,i});
                    currentMove++;
                }
            }
        }
    }
}

//Apply the solution to the renderer.
void DrawPathways()
{
    if(tourIsFinished)
    {
        SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
        SDL_Point _points[cb_dim_x * cb_dim_y];

        for(int i = 0; i < cb_dim_x * cb_dim_y; i++)
        {
            _points[i] = cb_points[i];

            _points[i].x = tileRect.x + (tileRect.w * _points[i].x) + (0.5f * tileRect.w);
            _points[i].y = tileRect.y + (tileRect.h * _points[i].y) + (0.5f * tileRect.h);


            SDL_Rect r;
            r.w = 6;
            r.h = 6;
            r.x = _points[i].x - r.w/2.0f;
            r.y = _points[i].y - r.h/2.0f;

            SDL_RenderFillRect( renderer, &r );
            SDL_RenderPresent(renderer);
            SDL_SetRenderDrawColor( renderer, 255,255,255,255 );

        }

        SDL_RenderDrawLines(renderer, _points, cb_dim_x * cb_dim_y);
    }
}

//Start the SDL rendering phase.
void BeginDrawPhase()
{
    SDL_PollEvent(&event);
    SDL_SetRenderTarget(renderer, texture);
    SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, 0x00);
    SDL_RenderClear(renderer);
}

//Output the renderer to the screen.
void EndDrawPhase()
{
    SDL_SetRenderTarget(renderer, NULL);
    SDL_RenderCopy(renderer, texture, NULL, NULL);
    SDL_RenderPresent(renderer);
}

//Initialize the SDL related components.
int initSDL()
{
        if (SDL_Init(SDL_INIT_VIDEO) < 0) {
                SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Couldn't initialize SDL: %s\n", SDL_GetError());
                return 3;
        }

        window = SDL_CreateWindow("SDL_CreateTexture",
                        SDL_WINDOWPOS_UNDEFINED,
                        SDL_WINDOWPOS_UNDEFINED,
                        SCREEN_WIDTH, SCREEN_HEIGHT,
                        SDL_WINDOW_SHOWN);

        SDL_SetWindowTitle(window, "KNIGHTS TOUR (SDL2/C++) [Nick Kortleve]");

        tileRect.x=0;
        tileRect.y=0;
        tileRect.w = SCREEN_WIDTH;
        tileRect.h = SCREEN_HEIGHT;

        renderer = SDL_CreateRenderer(window, -1, 0);
        texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, SCREEN_WIDTH, SCREEN_HEIGHT);
}

//Wrapper function to be called from main
void DrawChessboard()
{
    DrawChessboard(cb_dim_x, cb_dim_y, renderer, texture);
}

//Wrapper function to be called from main
void CalculateBoardTile()
{
    CalculateBoardTile(&tileRect, cb_dim_x, cb_dim_y);
}

// Calculate the size of the chessboard based on resolution
void CalculateBoardTile(SDL_Rect * rect, int xSize, int ySize)
{
    rect->x = cb_border;
    rect->y = cb_border;

    if(rect->h * ySize > (SCREEN_HEIGHT - (2*cb_border)) )
    {
        rect->h = (float)(SCREEN_HEIGHT - (2*cb_border)) / ySize;
        rect->w = (float)(SCREEN_HEIGHT - (2*cb_border)) / ySize;
    }

    else if(rect->w * xSize > SCREEN_WIDTH - (2 * cb_border))
    {
        rect->h = (float)(SCREEN_WIDTH - (2 * cb_border)) / xSize;
        rect->w = (float)(SCREEN_WIDTH - (2 * cb_border)) / xSize;
    }

    rect->x = (SCREEN_WIDTH - (rect->w * xSize))/2.0f;
}

// Draw the chessboard components to the renderer.
void DrawChessboard(int xSize, int ySize, SDL_Renderer * renderer, SDL_Texture * texture)
{
    tempTileRect = tileRect;
    bool alternateY = false;

    for(int i = 0; i < ySize; i++)
    {
        for (int j = 0; j < xSize; j++)
        {
            SDL_RenderDrawRect(renderer,&tempTileRect);

            if( alternateY ? j % 2 : !(j % 2))
            {
                SDL_SetRenderDrawColor(renderer, 0x4A, 0x1D, 0x0F, 0x00);
            } else
            {
                SDL_SetRenderDrawColor(renderer, 0xE9, 0xAD, 0x9B, 0x00);
            }

            SDL_RenderFillRect(renderer, &tempTileRect);
            tempTileRect.x += tempTileRect.w;
        }

        tempTileRect.x = tileRect.x;
        tempTileRect.y += tempTileRect.h;
        alternateY = !alternateY;
    }
}

// Destroy all SDL related components from memory.
void DestroySDLComponents()
{
    SDL_DestroyRenderer(renderer);
    SDL_DestroyTexture(texture);
    SDL_Quit();
}

