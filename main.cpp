#include <iostream>
#include <stdio.h>
#include <string>

#include "SDL2/SDL.h"
#include "SDL2/SDL_ttf.h"

#include "tour.h"
#include "chessboard.h"

int cb_dim_x = 5;
int cb_dim_y = 5;
int kn_pos_x = 2;
int kn_pos_y = 3;

// Show the user how to start the program from the command line.
static void ShowUsage()
{
    std::cerr << "Usage: SDL_KnightsTour.exe <BoardSize> <KnightsPosition>" << std::endl
              << "\t-b=#\t\tSpecify the board size\n"
              << "\t\t\tUsage:\t-b=5\n"
              << "\t-k=#A\t\tSpecify the knights position\n"
              << "\t\t\tUsage:\t-k=2c\n"
              << std::endl;
}

//Parse the program's arguments into the fields.
bool ParseArguments(char* _argv[])
{
        if(_argv[1][0] == '/' && (_argv[1][1] == '?' || _argv[1][1] == 'h'))
        {
            ShowUsage();
        }
        std::cout << std::endl << "argv1: " + std::string(_argv[0]) << std::endl << "argv2: " + std::string(_argv[1]) << std::endl << "argv3: " + std::string(_argv[2]) << std::endl << std::endl;

        if(_argv[1][0] == '-' && _argv[1][1] == 'b' && _argv[1][2] == '=' &&
           (_argv[1][3] > '1' && _argv[1][3] <= '9') && _argv[1][4] == '\0')
        {
                cb_dim_x = _argv[1][3] - '0';
                cb_dim_y = _argv[1][3] - '0';
        }
        else
        {
            std::cout << "Parsing error of boardsize!\nSee usage for examples.";
            return false;
        }

        if(_argv[2][0] == '-' && _argv[2][1] == 'k' && _argv[2][2] == '=' &&
           (_argv[2][3] >= '0' && _argv[2][3] < cb_dim_x + '0') && (_argv[2][4] >= 'a' && _argv[2][4] < cb_dim_y+ 'a') &&
           _argv[2][5] == '\0' )
        {
                kn_pos_x = _argv[2][3] - '1';
                kn_pos_y = _argv[2][4] - 'a';
        }
        else
        {
            std::cout << "Parsing error of knight position!\nSee usage for examples.";
            return false;
        }

        std::cout << "Tour settings: (" << cb_dim_x << "x" << cb_dim_y << ")" << " @ " << kn_pos_x + 1 << (char)(kn_pos_y + 'a') <<std::endl;
        return true;
}

// Entry point of application.
int main(int argc, char* argv[])
{
    std::cout << "Welcome to C++ Knight's Tour" << std::endl;
    if (argc < 3) {
        ShowUsage();
        return 1;
    } else {
        if(!ParseArguments(argv))
        {
            cin.get();
            return 0;
        }
    }

    if( int status = initSDL() == 3 ) {return status;}

    CalculateBoardTile();

    bool tourHasStarted = false;

    // Main draw routine of application.
    // Draws the chessboard once before and once after the tour.
    while (true) {

        BeginDrawPhase();
        DrawChessboard();
        DrawPathways();
        EndDrawPhase();

        if(tourIsFinished)
            break;

        if(!tourHasStarted)
        {
            std::cout << std::endl << "Starting calculation..." << std::endl << std::endl;
            // Create a tour object start self-starts the knight's tour.
            tour T(cb_dim_x, kn_pos_x, kn_pos_y);
            tourHasStarted = true;
        }
    }

    // Wait for the user to exit the application.
    while(true) {
        SDL_PollEvent(&event);
        switch(event.type) {
            case SDL_QUIT:
                DestroySDLComponents();
                return 0;
            default:
                SDL_Delay(50);
        }
    }
    return 0;
}
