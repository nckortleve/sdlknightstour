#ifndef CHESSBOARD_H_INCLUDED
#define CHESSBOARD_H_INCLUDED

#include <iostream>
#include <stdio.h>
#include <string>

#include "SDL2/SDL.h"

void DrawChessboard(int xSize, int ySize, SDL_Renderer * renderer, SDL_Texture * texture);
void CalculateBoardTile(SDL_Rect * tileRect, int xSize, int ySize);
void ConvertSolutionToPoints(std::vector< std::vector< int > > cb);

int initSDL();
void DestroySDLComponents();

void CalculateBoardTile();
void DrawChessboard();
void DrawPathways();
void BeginDrawPhase();
void EndDrawPhase();

const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;

const int cb_border = 20;

extern int cb_dim_x;
extern int cb_dim_y;

extern int kn_pos_x;
extern int kn_pos_y;

extern SDL_Window *window;
extern SDL_Renderer *renderer;
extern SDL_Surface *surf;
extern SDL_Texture *texture;
extern SDL_Texture *image;

extern SDL_Event event;

extern SDL_Rect tempTileRect;
extern SDL_Rect tileRect;

extern SDL_Point points[];

#endif // CHESSBOARD_H_INCLUDED
